# Research

- [performance characteristics](perf.md)
- [Task Information & Volume Analysis for Workflow Input/Output](task_info.md)
- [Workflow Simulator for Co-scheduling](/examples/co-scheduling/)
- [Comparison between file-based and adios](examples/co-scheduling/benchmark.md)

