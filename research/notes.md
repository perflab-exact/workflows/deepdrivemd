-*-Mode: markdown;-*-
-----------------------------------------------------------------------------

Comparing ADIOS and RADICAL
=============================================================================

RADICAL
----------------------------------------

1. Scheduling: RADICAL schedules tasks as dependences are resolved

   Dependencies have one source and one sink. This means that any
   conceptual joins or forks must be appropriately converted and the
   join/fork operator implemented by the user as an
   aggregator/duplicator.
   
   Tasks are nodes. Files are edges.
   
   > Radical provides a callback to make sure `n` tasks are finished before moving to the next phase, i.e., EnTK's stage post_exec, and the related example is [here](https://radicalentk.readthedocs.io/en/stable/adv_examples/adapt_ta.html). However, this feature isn't exactly to permit a "dependence". Radical model provides a task placement only, and the data management is responsible by a user application, how to implement coupling or dependence.


2. Task assignent:
   - By default, tasks execute on any available node
   - User can assign tags to tasks and request that only certain nodes
     execute certain tags.


ADIOS (+ RADICAL) model
----------------------------------------

1. Scheduling:
   - Reserve some nodes for ADIOS servers
   - RADICAL schedules tasks as dependences are resolved (bp-file, ADIOS 1)

2. Dependencies are implemented with producer/consumer pipes (put/get)
   with one producer and one consumer.

   - Producers block until their value is copied to the pipe

   - Consumers poll (block) until a new value (record) is available.
     No notify as in publish/subscribe.
     
     > Check for new value: [the read_step method](https://github.com/DeepDriveMD/DeepDriveMD-pipeline/blob/c0073303a824b66fe1d0b64a53ad76bfde223848/deepdrivemd/data/stream/adios_utils.py#L44) and [the adios BeginStep](https://adios2.readthedocs.io/en/latest/components/components.html?#beginstep)
     
   - Pipes are implemented with special files (bp-file, ADIOS 1)
  
3. A pipe can hold multiple producer values. This means producers can
   run at different rates than consumers. One can set a limit on the
   pipe buffers, after which producer operations block (or drop
   values).
   
   Implication: ADIOS could permit some tasks to complete more quickly
   than with RADICAL, but the overall logical critical path could
   easily be the same.
   
   Note: In this ADIOS version, there is one bp-file per file in
   RADICAL version. However, multiple tasks could use same bp-file.

4. At joins, consumers must check that all incoming dependence edges
   have been resolved.

[[Question]] If RADICAL is managing scheduling, why are there aggregators that poll on their source data?
> While the simulations keep running in the ADIOS model, the aggregator needs to find out if there are new data to process proactively i.e., polling. In RADICAL only model, the aggregator presumes that new data (files) produced from the simulation, as earlier tasks (simulation) were terminated to come to itself in the workflow pipeline. 
